/*
 * main.cpp
 *
 *  Created on: Oct 11, 2017
 *      Author: Tyler Schicke
 */

#include <iostream>
#include <fstream>
#include <SFML/Window.hpp>
#include <gl/glew.h>
#include <gl/gl.h>
#include <glm/glm.hpp>

namespace ts {
	
	int loadTempShader(std::string path, int shaderType) {
		std::string shaderCode;
		
		std::ifstream shaderStream(path, std::ios::in);
		if (shaderStream.is_open()) {
			std::string line = "";
			while (getline(shaderStream, line)) {
				shaderCode += '\n' + line;
			}
			shaderStream.close();
		} else {
			std::cerr << "Couldn't open file\n";
			return -1;
		}
		
		int shaderID = glCreateShader(shaderType);
		
		const char * sourcePointer = shaderCode.c_str();
		glShaderSource(shaderID, 1, &sourcePointer, NULL);
		glCompileShader(shaderID);
		
		int compilationStatus = 0;
		glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compilationStatus);
		
		if (compilationStatus == GL_FALSE) {
			char infoLog[1024];
			int logLength;
			glGetShaderInfoLog(shaderID, 1024, &logLength, infoLog);
			std::cerr << "Error! Shader File " << path << " wasn't compiled! The compiler returned:\n\n" << infoLog << '\n';
			glDeleteShader(shaderID);
			return -1;
		}
		
		return shaderID;
	}
	
	int loadShader(std::string vertexPath, std::string fragmentPath) {
		int vertexShader = loadTempShader(vertexPath, GL_VERTEX_SHADER);
		if(vertexShader == -1) {
			return -1;
		}
		int fragmentShader = loadTempShader(fragmentPath, GL_FRAGMENT_SHADER);
		if(fragmentShader == -1) {
			glDeleteShader(vertexShader);
			return -1;
		}
		
		int programID = glCreateProgram();
		glAttachShader(programID, vertexShader);
		glAttachShader(programID, fragmentShader);
		
		glLinkProgram(programID);
		int linkStatus = 0;
		glGetProgramiv(programID, GL_LINK_STATUS, &linkStatus);
		if(linkStatus != GL_TRUE) {
			glDeleteShader(vertexShader);
			glDeleteShader(fragmentShader);
			glDeleteProgram(programID);
			return -1;
		}
		return programID;
	}
	
	int makePlane(GLuint *vao, GLuint *indexArray) {
		float quad[] = {
			-1, -1, 0,
			-1, 1, 0,
			1, 1, 0,
			1, -1, 0,
		};
		unsigned int indices[] = {
			0, 1, 2,
			0, 2, 3
		};
		
		GLuint vertexArray;
		glGenBuffers(1, &vertexArray);
		glGenBuffers(1, indexArray);
		glGenVertexArrays(1, vao);
		
		glBindVertexArray(*vao);
		
		glBindBuffer(GL_ARRAY_BUFFER, vertexArray);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *indexArray);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
		
		glBindVertexArray(0);
		return 0;
	}
	
}

using namespace ts;

int main()
{
	sf::Window window(sf::VideoMode(900, 600), "Mandelbrot");
	sf::ContextSettings settings = window.getSettings();
	std::cout << settings.majorVersion << "." << settings.minorVersion << std::endl;
	if(glewInit() != GLEW_OK) {
		return -1;
	}
	int shader = loadShader("shaders/quad.vert", "shaders/mandelbrot.frag");
	if(shader == -1) {
		std::cout << "Shader failed to load" << std::endl;
	} else {
		std::cout << "Shader loaded successfully" << std::endl;
	}
	GLuint vertexArray;
	GLuint indexArray;
	makePlane(&vertexArray, &indexArray);
	glUseProgram(shader);
	GLint sizeLoc = glGetUniformLocation(shader, "WindowSize");
	GLint scaleLoc = glGetUniformLocation(shader, "scale");
	GLint offsetLoc = glGetUniformLocation(shader, "offset");
	GLint cLoc = glGetUniformLocation(shader, "c");
	GLint mandelLoc = glGetUniformLocation(shader, "mandel");
	glm::vec2 offset, c;
	float scale = 1;
	bool useMandel = false;
	glUniform2f(sizeLoc, 900, 600);
	glUniform1f(scaleLoc, 1);
	glUniform2f(offsetLoc, 0, 0);
	glClearColor(1, 0, 0, 1);
	while(window.isOpen()) {
		sf::Event event;
		while(window.pollEvent(event)) {
			if(event.type == sf::Event::Closed) {
				window.close();
			} else if(event.type == sf::Event::KeyPressed) {
				if(event.key.code == sf::Keyboard::A) {
					offset.x -= 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::D) {
					offset.x += 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::W) {
					offset.y += 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::S) {
					offset.y -= 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::Left) {
					c.x -= 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::Right) {
					c.x += 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::Up) {
					c.y += 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::Down) {
					c.y -= 0.1 * scale;
				} else if(event.key.code == sf::Keyboard::R) {
					scale /= 1.1;
				} else if(event.key.code == sf::Keyboard::F) {
					scale *= 1.1;
				} else if(event.key.code == sf::Keyboard::M) {
					useMandel = !useMandel;
				}
				glUniform1f(scaleLoc, scale);
				glUniform2f(offsetLoc, offset.x, offset.y);
				glUniform2f(cLoc, c.x, c.y);
				glUniform1i(mandelLoc, useMandel);
			}
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		glBindVertexArray(vertexArray);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexArray);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		window.display();
		sf::sleep(sf::milliseconds(10));
	}
	return 0;
}

