#version 400

uniform vec2 WindowSize;
uniform float scale;
uniform vec2 offset;
uniform vec2 c;
uniform int mandel;

out vec4 fragColor;

vec2 multComplex(vec2 a, vec2 b) {
	return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

vec2 squareComplex(vec2 v) {
	return vec2(v.x * v.x - v.y * v.y, 2 * v.x * v.y);
}

float length2(vec2 v) {
	return dot(v, v);
}

vec4 getColor(int n, int limit) {
	return vec4((-cos(0.025*float(n))+1.0)/2.0,
				(-cos(0.08*float(n))+1.0)/2.0,
				(-cos(0.12*float(n))+1.0)/2.0,
				1);
}

int mandelbrot(vec2 startPoint, int limit) {
	vec2 f = vec2(0, 0);
	for(int i = 0; i < limit; ++i) {
		f = squareComplex(f) + startPoint;
		if(length2(f)>4) {
			return i;
		}
	}
	return 0;
}

int julia(vec2 startPoint, vec2 c, int limit) {
	vec2 f = startPoint;
	for(int i = 0; i < limit; ++i) {
		f = squareComplex(f) + c;
		if(length2(f)>4) {
			return i;
		}
	}
	return 0;
}

vec2 getComplexPoint(vec2 screenPoint) {
	float minV = min(WindowSize.x, WindowSize.y);
	vec2 normalized = screenPoint / vec2(minV);
	float range = 2.5 * scale;
	return vec2(normalized * range * 2) - vec2(range * WindowSize.x / minV, range * WindowSize.y / minV) + vec2(offset);
}

void main() {
	vec2 center = getComplexPoint(gl_FragCoord.xy);
	vec2 c2 = getComplexPoint(gl_FragCoord.xy + vec2(0.25, 0.25));
	vec2 c3 = getComplexPoint(gl_FragCoord.xy + vec2(-0.25, 0.25));
	vec2 c4 = getComplexPoint(gl_FragCoord.xy + vec2(-0.25, -0.25));
	vec2 c5 = getComplexPoint(gl_FragCoord.xy + vec2(0.25, -0.25));
	int limit = 1000;
	int i2, i3, i4, i5;
	//int i = mandelbrot(c, limit);
	if(mandel == 0) {
		if(length2(center - c) < 0.001 * scale * scale) {
			fragColor = vec4(1, 1, 1, 1);
			return;
		}
		//i2 = mandelbrot(c2, limit);
		//i3 = mandelbrot(c3, limit);
		//i4 = mandelbrot(c4, limit);
		//i5 = mandelbrot(c5, limit);
		i2 = mandelbrot(center, limit);
	} else {
		//i2 = julia(c2, c, limit);
		//i3 = julia(c3, c, limit);
		//i4 = julia(c4, c, limit);
		//i5 = julia(c5, c, limit);
		i2 = julia(center, c, limit);
	}
	//fragColor = getColor(i, limit);
	fragColor = getColor(i2, limit);
	//fragColor += getColor(i3, limit);
	//fragColor += getColor(i4, limit);
	//fragColor += getColor(i5, limit);
	//fragColor /= 4;
}
